#!/usr/bin/env bash
#shellcheck disable=SC2034
VLO_VERSION="4.12.1"
REMOTE_RELEASE_URL="https://github.com/clarin-eric/VLO/releases/download/${VLO_VERSION}/vlo-${VLO_VERSION}-docker.tar.gz"
NAME="vlo-${VLO_VERSION}-docker"
